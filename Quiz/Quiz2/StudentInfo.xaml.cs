﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2
{
    /// <summary>
    /// StudentInfo.xaml 的交互逻辑
    /// </summary>
    public partial class StudentInfo : Window
    {

        StudentEntities student = new StudentEntities();
        public StudentInfo()
        {
            InitializeComponent();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            txtFname.Text = "";
            txtId.Text = "";
            txtLname.Text = "";
            cbRigister.IsChecked = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtFname.Text.Trim() == "" ||txtLname.Text.Trim() == "")
            {
                MessageBox.Show("Please enter all fields");
                return;
            }
            else
            {

                try
                {
                    StudentInfo stu = new StudentInfo()
                {
                    //Id = Int32.Parse(txtId.Text.Trim()),
                    FirstName = txtFname.Text.Trim(),
                    LastName = txtLname.Text.Trim(),
                    IsRegistered = Boolean.Parse(cbRigister.IsChecked == true ? "true" : "false")
                };

                    student.StudentInfoes.Add(stu);
                    student.SaveChanges();
                    loadgrid();
                    MessageBox.Show("Added students");
                }
                catch(Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
                
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadgrid();
        }

        public void loadgrid()
        {
            var data = from s in student.StudentInfoes select s;
            dgdStudentList.ItemsSource = data.ToList();
        }
    }
}
