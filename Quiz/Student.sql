create database Student;
Use Student;

drop table if exists StudentInfo;
create table StudentInfo 
(
	Id int identity(1,1) not null,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50) not null,
	IsRegistered bit
	constraint pk_Id primary key clustered
	(
		Id asc
	)
);

insert into StudentInfo(FirstName, LastName, IsRegistered) values('aa', '11', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ab', '12', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ac', '13', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ad', '14', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ae', '15', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('af', '16', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ag', '17', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ah', '18', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('ai', '19', 'false');
insert into StudentInfo(FirstName, LastName, IsRegistered) values('aj', '10', 'false');