﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuizTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> intList = new List<int>();
            intList.Add(55);
            intList.Add(200);
            intList.Add(740);
            intList.Add(76);
            intList.Add(230);
            intList.Add(482);
            intList.Add(95);
            Console.WriteLine("The members of the list are:");
            foreach(var members in intList)
            {
                Console.Write($"{members} ");
            }

            List<int> checkList = intList.FindAll(i => i > 80 ? true : false);
            Console.WriteLine("\nThe numbers greater than 80 are:");

            foreach(var check in checkList)
            {
                Console.WriteLine(check);
            }
        }
    }
}
